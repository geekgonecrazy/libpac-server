var api = [];

require("fs").readdirSync("./api").forEach(function(file) {
	var moduleName = file.replace('.js', '');
	if (file.charAt(0) != '.' && moduleName !== 'index') {
		api[moduleName] = require("./" + file);
	}
});	

module.exports = api;