var winston = require('winston');

module.exports = function () {
	var logger = new (winston.Logger)({
		transports: [
			new (winston.transports.Console)({
				colorize: true,
				prettyPrint: true
			}),
			new (winston.transports.File)({
				filename: 'app.log'
			})
		],
		exceptionHandlers: [
			new winston.transports.File({
				filename: 'error.log'
			}),
			new (winston.transports.Console)({
				colorize: true,
				prettyPrint: true
			}),
		]
	});
	
	return logger;
}