module.exports = function (app, router) {
	var routes = {};
	
	require("fs").readdirSync("./routes").forEach(function(file) {
		if (file.charAt(0) != '.' && file !== 'index.js') {
			var routes = require("./" + file)(app);
			for (var path in routes) {
				var route = routes[path];
				
				if (typeof route == 'function') {
					addRoute(path, 'get', [], route);	
				} else if (typeof route == 'object') {
					if (route.methods) {
						route.methods.forEach(function (method) {
							addRoute(path, method, route.middleware || [], route.fn);
						});
					}
				} else {
					console.log(file + 'in inproper format for routes');	
				}
			}
		}
	});
	
	function addRoute(path, method, middleware, fn) {
		router[method](path, middleware, fn);
	}	
	
	/*app.events.on('router:request', function(req, res) {
		var routes = app.routes[req.method.toLowerCase()];
		routes.forEach(function(route) {
			var regx = new RegExp(route.regexp);
			if (regx.test(req.url)) {
				var matching = req.url.match(route.regexp);
				if (matching.length > 1) {
					matching.shift();
					matching.forEach(function(param, index) {
						req.params[route.keys[index].name] = param;
					});
				}
				
				// Execute middleware.
				var callbacks = route.callbacks;
				for (var i in callbacks) {
					if (i != callbacks.length-1) {
						callbacks[i](req, res, function() {
							console.log('Executed middleware');
						});
					} else {
						callbacks[i](req, res);	
					}
				}
			}
		});
	});*/
}

