var express = require('express');
var connect = require('connect');
var http = require('http');

var app = express();
var server = http.createServer(app);

app.use(connect.urlencoded());
app.use(connect.json());
app.use(connect.cookieParser());

// Attach Event Emitter to App Object
var EventEmitter = require('events').EventEmitter;
app.events = new EventEmitter();

app.Q = require('q');
app.async = require('async');

// Some utils and our logger
app.util = require('./util');
app.log = require('./logger')();

app.configured = true;	

try {
	app.config = require('./config');
} catch (e) {
	if (e.code === 'MODULE_NOT_FOUND') {
		app.log.info('Server not configured');
		app.configured = false;
	}
} 

app.database = require('./database');

if (app.configured) {
	app.database.connect(app, app.config.host, app.config.port, app.config.database, app.config.username, app.config.password).then(function () {
		app.models = require('./models')(app);
	}, function (err) {
		app.log.info(err);
	});
}

app.api = require('./api');

var apiRouter = express.Router();

require('./routes/')(app, apiRouter);
app.use('/api/', apiRouter);

var openConnections = [];

app.get('/events', function (req, res) {
	req.socket.setTimeout(Infinity);
	
	res.writeHead(200, {
		'Content-Type': 'text/event-stream',
		'Cache-Control': 'no-cache',
		'Connection': 'keep-alive'
	});
	
	res.write('\n');
	
	openConnections.push(res);
	
	req.on('close', function () {
		var toRemove;
		
		for (var i=0; i<openConnections.length; i++) {
			if (openConnections[i] === res) {
				toRemove = i;
				break;
			}
		};
		
		openConnections.splice(openConnections, 1);
	});
});

setInterval(function() {
    // we walk through each connection
    openConnections.forEach(function(resp) {
        var d = new Date();
        resp.write('id: ' + d.getMilliseconds() + '\n');
        resp.write('data:' + 'test' + '\n\n'); // Note the extra newline
    });
 
}, 1000);

app.use(express.static(__dirname + '/webapp'));

//require('./socketHook')(app);



app.log.info('Server Started');
			 
server.listen(3000);

module.exports = app; 