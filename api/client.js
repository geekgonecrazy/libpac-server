exports.register = function (app, req) {
	var defer = app.Q.defer();
	
	app.log.info('client register');
	console.log(req.body);
	
	var body = req.body;
	
	if (body.verification === 'D154') {
        defer.resolve({success: true});
	} else {
        defer.resolve({success: false, message: 'Invalid Code'});
	}
	
	
	return defer.promise;
}

exports.test = function (app) {
	app.log.info('Computers Test');
}