module.exports = function(mongoose) {
	
	var settings = new mongoose.Schema({
		name: String
	});
	
	return mongoose.model('settings', settings);
};