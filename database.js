var mongoose = require('mongoose');
exports.connect = function (app, host, port, db, username, password) {
	var defer = app.Q.defer();
	var connect_string = 'mongodb://';
	
	if (username && password) {
		connect_string += username + ':'+password + '@';
	}
	
	connect_string += host + ':'+port+ '/'+db;
	
	if (!app.mongoose) {
	
		mongoose.connect(connect_string, function (err) {
			if (err) {
				app.log.error(err);
				defer.reject(err);
			} else {
				app.mongoose = mongoose;
				defer.resolve();
			}
		});
	} else {
		defer.resolve();
	}
	
	return defer.promise;
};