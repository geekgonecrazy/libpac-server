exports.get = function (app, req) {
	var defer = app.Q.defer();
	
	app.log.info('Computers GET');
	
	defer.resolve([{name: 'Computer 1', status: 'online'},
		{name: 'Computer 2', status: 'offline'},
		{name: 'Computer 3', status: 'logged_in', guestName: 'Guest 1', timeRemaining: 58},
		{name: 'Computer 4', status: 'error'}]);
	
	return defer.promise;
};

exports.test = function (app) {
	app.log.info('Computers Test');
};