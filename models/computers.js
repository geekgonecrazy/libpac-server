module.exports = function(mongoose) {
	
	var computers = new mongoose.Schema({
		name: String
	});
	
	return mongoose.model('computers', computers);
};