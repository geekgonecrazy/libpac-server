module.exports = function (app) {
	return {
		'/computers' : {
			methods : ['get', 'post'],
			middleware : [testmiddleware],
			fn : function (req, res) {
				app.api.computers.get(app, req).then(function (message) {
					res.json(message);
				});
				
			}
		},
		
		'/computers/1' : {
			methods : ['get', 'post'],
			middleware : [testmiddleware],
			fn : function (req, res) {
				console.log('SessionID:', req.sessionID);
				req.session.test = 1;
				req.session.save(function(err) {console.log(err); });
				console.log(req.session);
				res.send('we have socket.io and reqular routes combined!!!');
			}
		},
		
		'/computers/1/test' : {
			methods : ['post'],
			fn : function (req, res) {
				res.send({besttest:true});
			}
		},
		
		'/client/register' : {
            methods : ['post'],
            fn : function (req, res) {
                app.api.client.register(app, req).then(function (message) {
                    res.send(message);
                });
            }
		}
		
	};
};

function testmiddleware (req, res, next) {
	console.log('test middleware');
	next();
}