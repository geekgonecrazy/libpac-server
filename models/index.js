module.exports = function (app) {
	var models = [];

	require("fs").readdirSync("./models").forEach(function(file) {
		var moduleName = file.replace('.js', '');
		if (file.charAt(0) != '.' && moduleName !== 'index') {
			var model = require('./' + file)(app.mongoose);
			
			models[moduleName] = model;
		}
	});	

	return models;
}